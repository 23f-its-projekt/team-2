The script:
```bash

#!/bin/bash

#######################################

# POC for UCL IT security School project og how automation of CIS benchmarks could be done when you dont want to pay $$$$$ to CIS for their scripts. But you still great CIS.
# Follow CIS benchmarks for securing SSH and very little privilege escalation.
# All Credits and inspiration goes to sae13. Check out on github: https://github.com/sae13/debian-11-hardening

#######################################

# Colours
red="$(tput setaf 1)"
green="$(tput setaf 2)"
orange="$(tput setaf 3)"
white="$(tput sgr0)"

clear

# Tell user to open another terminal. Some commands must be configured manual.
while true; do
	read -r -p "Please open another terminal. Some security meassures requires MANUAL change. The program will stop and tell you when it reaches them. Are you ready to proceed? (Y/N)" answer
	case $answer in
		 [Yy]* ) echo "Great. Script will now start the hardening.."; break ;;
		 [Nn]* ) echo "No worries. To prepare please read the CIS 18 controls/benchmarks before running this script to get a overview of the security meassures." ; exit 0 ;;
		  * ) echo "Please answer Y or N.";;
	esac
done

# Function that applies Security meassure. It will respond with either FAILED or PASS
hardening() {
    echo ------------------------------
    echo
    echo "${orange}$4${white}"
    echo

    echo "${orange}running Audit test:${white} $2"

    if eval "$2 | grep -i $3"; then
        echo
        echo "${green}PASSED!${white}"
    else
        echo "${orange}hardening:${white} $1"
        if eval "$1"; then
            if eval "$2 | grep -i $3"; then
                echo
                echo "${green}PASSED!${white}"
            else
                echo
                echo "${red}FAILED!${white}"
                echo
                exit 1
            fi
        else
            echo
            echo "${red}FAILED!${white}"
            echo
            exit 1
        fi
    fi
    echo
}

install_required_packages () {
# Update your system before starting the configurations
echo -e "$orange Updating your system $white"
apt-get update && apt-get upgrade -y

# Install required software packages
echo -e "$orange Installing packages: openssh-server $white"
apt-get install openssh-server
}
configure_and_setup_ssh () {
# Create a backup of sshd_config file
back_up=$(date +sshd_bakup_%s)
cp /etc/ssh/sshd_config "$HOME/$back_up"

# 5.2.1
echo "$green 5.2.1 Ensure permissions on /etc/ssh/sshd_config are configured (Scored) "
hardening 'chown root:root /etc/ssh/sshd_config' 'stat /etc/ssh/sshd_config|grep "Access: ("' '".*root.*root"' '5.2.1 Ensure permissions on /etc/ssh/sshd_config are configured (Scored)'
hardening 'chmod 0600 /etc/ssh/sshd_config' 'stat /etc/ssh/sshd_config|grep "Access: ("' '"0600"' '5.2.1 Ensure permissions on /etc/ssh/sshd_config are configured (Scored)'
# 5.2.2
echo "$green 5.2.2 Ensure permissions on SSH private host key files are configure (Scored)"
echo "$red ##USER May Need Check Manually $white"
echo "$orange find /etc/ssh -xdev -type f -name 'ssh_host_*_key' -exec chown root:root {}\; $white"
find /etc/ssh -xdev -type f -name 'ssh_host_*_key' -exec chown root:root {} \;
echo "$orange find /etc/ssh -xdev -type f -name 'ssh_host_*_key' -exec chmod 0600 {} \; $white"
find /etc/ssh -xdev -type f -name 'ssh_host_*_key' -exec chmod 0600 {} \;
# 5.2.3
echo "$green 5.2.3 Ensure permissions on SSH public host key files are configured (Scored)"
echo "$red ##USER May Need Check Manually $white"
echo "$orange find /etc/ssh -xdev -type f -name 'ssh_host_*_key.pub' -exec chmod go-wx {} \; $white"
find /etc/ssh -xdev -type f -name 'ssh_host_*_key.pub' -exec chmod go-wx {} \;
echo "$orange find /etc/ssh -xdev -type f -name 'ssh_host_*_key.pub' -exec chown root:root {} \; $white"
find /etc/ssh -xdev -type f -name 'ssh_host_*_key.pub' -exec chown root:root {} \;

# 5.2.4
echo "$green 5.2.4 Ensure SSH access is limited (Scored)"
echo "$red ##USER MUST DO THIS PART MANUALLY $white"
echo "Edit the /etc/ssh/sshd_config file to set one or more of the parameter as follows:"
echo ""
echo "AllowUsers <userlist>"
echo "AllowGroups <grouplist>"
echo "DenyUsers <userlist>"
echo "DenyGroups <grouplist>"
echo "Example: "
echo "AllowUsers ulrik john dwaynejohnson"
echo "DenyGroups Lapsus$"
echo ""
while true; do
	read -r -p "Have you configured it ?(Y/N): " answer
	case $answer in
		 [Yy]* ) echo "Then moving on to next security measure.." ; break ;;
		 [Nn]* ) echo "Come back to the script when your are done";  exit 0;;
		  * ) echo "Please answer Y or N.";;
	esac
done
# 5.2.5
hardening 'sed -i   "s/^\s*UsePAM/\#UsePAM/gI" /etc/ssh/sshd_config;echo "UsePAM yes">>/etc/ssh/sshd_config;systemctl restart sshd;sleep 5;' 'sshd -T | grep -i UsePAM' '"UsePAM yes"' '5.2.5 Ensure SSH PAM is enabled (Scored)'
# 5.2.6
hardening 'sed -i   "s/^\s*loglevel/\#LogLevel/gI" /etc/ssh/sshd_config;echo "LogLevel VERBOSE">>/etc/ssh/sshd_config;systemctl restart sshd;sleep 5;' 'sshd -T | grep -i loglevel' 'VERBOSE' '5.2.6 Ensure SSH LogLevel is appropriate (Scored)'
# 5.2.7
hardening 'sed -i   "s/^\s*PermitRootLogin/\#PermitRootLogin/gI" /etc/ssh/sshd_config;echo "PermitRootLogin no">>/etc/ssh/sshd_config;systemctl restart sshd;sleep 5;' 'sshd -T | grep -i PermitRootLogin' 'no' '5.2.7 Ensure SSH PermitRootLogin is disabled (Scored)'
# 5.2.8
hardening 'sed -i   "s/^\s*HostbasedAuthentication/\#HostbasedAuthentication/gI" /etc/ssh/sshd_config;echo "HostbasedAuthentication no">>/etc/ssh/sshd_config;systemctl restart sshd;sleep 5;' 'sshd -T | grep -i HostbasedAuthentication' 'no' '5.2.8 Ensure SSH HostbasedAuthentication is disabled (Scored)'
# 5.2.9
hardening 'sed -i   "s/^\s*PermitEmptyPasswords/\#PermitEmptyPasswords/gI" /etc/ssh/sshd_config;echo "PermitEmptyPasswords no">>/etc/ssh/sshd_config;systemctl restart sshd;sleep 5;' 'sshd -T | grep -i PermitEmptyPasswords' 'no' '5.2.9 Ensure SSH PermitEmptyPasswords is disabled (Scored)'
# 5.2.10
hardening 'sed -i   "s/^\s*PermitUserEnvironment/\#PermitUserEnvironment/gI" /etc/ssh/sshd_config;echo "PermitUserEnvironment no">>/etc/ssh/sshd_config;systemctl restart sshd;sleep 5;' 'sshd -T | grep -i PermitUserEnvironment' 'no' '5.2.10 Ensure SSH PermitUserEnvironment is disabled (Scored)'
# 5.2.11
hardening 'sed -i   "s/^\s*IgnoreRhosts/\#IgnoreRhosts/gI" /etc/ssh/sshd_config;echo "IgnoreRhosts yes">>/etc/ssh/sshd_config;systemctl restart sshd;sleep 5;' 'sshd -T | grep -i IgnoreRhosts' 'yes' '5.2.11 Ensure SSH IgnoreRhosts is enabled (Scored)'
# 5.2.12
hardening 'sed -i   "s/^\s*X11Forwarding/\#X11Forwarding/gI" /etc/ssh/sshd_config;echo "X11Forwarding no">>/etc/ssh/sshd_config;systemctl restart sshd;sleep 5;' 'sshd -T | grep -i X11Forwarding' 'no' '5.2.12 Ensure X11 forwaing is disabled (Scored)'
# 5.2.13
hardening 'sed -i   "s/^\s*Ciphers\ /\#Ciphers\ /gI" /etc/ssh/sshd_config;echo "Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr">>/etc/ssh/sshd_config;systemctl restart sshd;sleep 5;' 'sshd -T | grep -i ciphers' '"chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr"' '5.2.13 Ensure only strong Ciphers are used (Scored)'
# 5.2.14
hardening 'sed -i   "s/^\s*MACs\ /\#MACs\ /gI" /etc/ssh/sshd_config;echo "MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-sha2-512,hmac-sha2-256">>/etc/ssh/sshd_config;systemctl restart sshd;sleep 5;' 'sshd -T | grep -i MACs' '"hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-sha2-512,hmac-sha2-256"' '5.2.14 Ensure only strong MAC algorithms are used (Scored)'
# 5.2.15
hardening 'sed -i   "s/^\s*KexAlgorithms\ /\#KexAlgorithms\ /gI" /etc/ssh/sshd_config;echo "KexAlgorithms curve25519-sha256,curve25519-sha256@libssh.org,diffie-hellman-group14-sha256,diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,ecdh-sha2-nistp521,ecdh-sha2-nistp384,ecdh-sha2-nistp256,diffie-hellman-group-exchange-sha256">>/etc/ssh/sshd_config;systemctl restart sshd;sleep 5;' 'sshd -T | grep -i KexAlgorithms' '"curve25519-sha256,curve25519-sha256@libssh.org,diffie-hellman-group14-sha256,diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,ecdh-sha2-nistp521,ecdh-sha2-nistp384,ecdh-sha2-nistp256,diffie-hellman-group-exchange-sha256"' '5.2.15 Ensure only strong Key Exchange algorithms are used (Scored)'
# 5.2.16
hardening 'sed -i   "s/^\s*AllowTcpForwarding/\#AllowTcpForwarding/gI" /etc/ssh/sshd_config;echo "AllowTcpForwarding no">>/etc/ssh/sshd_config;systemctl restart sshd;sleep 5;' 'sshd -T | grep -i AllowTcpForwarding' '"AllowTcpForwarding no"' '5.2.16 Ensure SSH AllowTcpForwarding is disabled (Scored)'
# 5.2.17
hardening 'sed -i   "s/^\s*Banner/\#Banner/gI" /etc/ssh/sshd_config;echo "Banner /etc/issue.net">>/etc/ssh/sshd_config;systemctl restart sshd;sleep 5;' 'sshd -T | grep -i Banner' '"Banner /etc/issue.net"' '5.2.17 Ensure SSH warning banner is configured (Scored)'
# 5.2.18
hardening 'sed -i   "s/^\s*MaxAuthTries/\#MaxAuthTries/gI" /etc/ssh/sshd_config;echo "MaxAuthTries 4">>/etc/ssh/sshd_config;systemctl restart sshd;sleep 5;' 'sshd -T | grep -i MaxAuthTries' '4' '5.2.18 Ensure SSH MaxAuthTries is set to 4 or less (Scored)'
# 5.2.19
hardening 'sed -i   "s/^\s*maxstartups/\#maxstartups/gI" /etc/ssh/sshd_config;echo "maxstartups 10:30:60">>/etc/ssh/sshd_config;systemctl restart sshd;sleep 5;' 'sshd -T | grep -i maxstartups' '"maxstartups 10:30:60"' '5.2.19 Ensure SSH MaxStartups is configured (Scored)'
# 5.2.20
hardening 'sed -i   "s/^\s*MaxSessions/\#MaxSessions/gI" /etc/ssh/sshd_config;echo "MaxSessions 10">>/etc/ssh/sshd_config;systemctl restart sshd;sleep 5;' 'sshd -T | grep -i MaxSessions' '"MaxSessions 10"' '5.2.20 Ensure SSH MaxSessions is limited (Scored)'
# 5.2.21
hardening 'sed -i   "s/^\s*LoginGraceTime/\#LoginGraceTime/gI" /etc/ssh/sshd_config;echo "LoginGraceTime 60">>/etc/ssh/sshd_config;systemctl restart sshd;sleep 5;' 'sshd -T | grep -i LoginGraceTime' '"LoginGraceTime 60"' '5.2.21 Ensure SSH LoginGraceTime is set to one minute or less (Scored)'
# 5.2.22
hardening 'sed -i   "s/^\s*ClientAliveInterval/\#ClientAliveInterval/gI" /etc/ssh/sshd_config;echo "ClientAliveInterval 300">>/etc/ssh/sshd_config;systemctl restart sshd;sleep 5;' 'sshd -T | grep -i ClientAliveInterval' '300' '5.2.22 Ensure SSH Idle Timeout Interval is configured (Scored)'
# 5.2.22
hardening 'sed -i   "s/^\s*ClientAliveCountMax/\#ClientAliveCountMax/gI" /etc/ssh/sshd_config;echo "ClientAliveCountMax 300">>/etc/ssh/sshd_config;systemctl restart sshd;sleep 5;' 'sshd -T | grep -i ClientAliveCountMax' '"ClientAliveCountMax 300"' '5.2.22 Ensure SSH Idle Timeout Interval is configured (Scored)'
}
configure_and_setup_privelege_escalation (){

# 5.3.1 Ensure sudo is installed
hardening 'apt-get update;apt-get install sudo -y' 'dpkg --get-selections | grep -v deinstall | grep sudo' '"install"' '5.3.1 Ensure sudo is installed (Scored)'
# 5.3.2 Ensure sudo commands use pty
hardening 'sed -i   "s/^\s*Defaults/\#Defaults/gI" /etc/sudoers;echo "Defaults use_pty">>/etc/sudoers;sleep 5;' 'cat /etc/sudoers | grep -i Defaults' '"Defaults use_pty"' '5.3.3 Ensure sudo commands use pty (Scored)'
# 5.3.3 Ensure sudo log file exists
hardening 'sed -i   "s/^\s*Defaults logfile/\#Defaults logfile/gI" /etc/sudoers;echo "Defaults logfile"=\"/var/log/sudo.log\"">>/etc/sudoers;sleep 5;' 'cat /etc/sudoers | grep -i "Defaults logfile"' '"Defaults logfile"=\"/var/log/sudo.log\"' '5.3.2 Ensure sudo log file exists (Scored)'
# TODO.. add missing privilege escalation benchmarks.....
}
# call functions one by one
install_required_packages
configure_and_setup_ssh
configure_and_setup_privelege_escalation

# Script is now done. Tell the user
echo "${green}Hardening complete. Script successfully ran through audit. Look through the terminal for status on PASS or FAILED during the audit${white}"

```
