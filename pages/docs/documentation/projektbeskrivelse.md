# Projektbeskrivelse

## Det udvalgte miljø:

Der tages udgangspunkt i netværksdiagrammet: ![netværksdiagram](../images/networkingdiagramv2.svg)

 Herudfra vælges der at demonstrere med afdelingerne:
Økonomi-Lan, Udvikling/test-Lan og Server-Lan

## Sådan vil vi opsætte miljøet:

### Virtualisering
Til miljøet bruges der virtuelle enheder. I virkelig praksis vil der have været brugt en blanding af både fysisk og virtuelt. Men på grund af begrænsninger og ressourcer, bruges kun virtuelt.
#### Værktøj
- Vmware Workstation
### Server opsætning
Til server brug har vi fokus på logning, monitoring af enheder og OS roller
##### Værktøj
- Wazuh
### Klienter
Til klienter bruges der linux. Vi har for det meste arbejdet med denne slags OS og derfor er det den valgte klient
##### Værktøj
- Linux distro (Som er baseret på Debian)
### Router:
Til vores netværk, opsættes en router. Dens funktion skal være at opdele virksomhedens netværk og kun tillade netværks kommunikation, ud fra den configuration som opsættes
#### Værktøj
- PFsense
