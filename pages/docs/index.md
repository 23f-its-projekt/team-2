# Link til gitlab page for dokumentation

[ Team 2 gitlab page](https://23f-its-projekt.gitlab.io/team-2)

# Baggrund
Hvad er baggrunden for projektet:

## Softwarevirksomhed
**Afdelinger:**

- Udvikling
- HR
- Økonomi
- Support
- It drift 

**produkt:**

- Web applikations sider


## Formål
Formålet med projektet er at skabe et overblik over en virksomheds netværk, hvilke services de benytter og hvordan sikkerheden i systemet bliver sat bedst muligt op.
## Mål
Projektet leverer følgende
### Nice to have
Enheds monitorering & logning
### Need to have
Netværksdiagram (Fysisk/Logisk)
Udvikle et til flere tekniske sikkerhedstiltag
Sikkerhedstest af tekniske produkter 

## Organisation
Hvem er medlemmerne i teamet og hvad er deres roller i projektet.
Trine Nissen
Ulrik Vinther
## Risikostyring

| Risiko beskrivelse     | Risko håndtering                                                                                                                                                      | Konsekvensskala | Risikoskala |
|------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------|-------------|
| Sygdom (kortvarig)     | Gruppen fortsætter arbejdet, og informere den fraværende om fremskridt i projektet.                                                                                   | 2               | 4           |
| Sygdom (langvarig)     | Gruppen fortsætter arbejdet, og informere den fraværende om fremskridt i projektet. Derudover kontakt med vejlederne omkring bedste fremgangsmåde ud fra situationen. | 5               | 1           |
| Transport forsinkelser | Kontakt gruppen omkring forsinkelser, og hvornår forventet ankomst er.                                                                                                | 1               | 4           |
| Computer problemer     | Backup af enheder, dokumenter mm. dokumentation af fremgangsmåde, så man effektivt kan genskabe tabt data mm.                                                         | 4               | 2           |

Inspiration hentet her https://www.cfl.dk/artikler/risikoanalyse

### Kommunikationskanaler
discord
### Kommunikationsaktiviteter
Møder

