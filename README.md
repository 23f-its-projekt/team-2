# Link til gitlab page for dokumentation

[ Team 2 gitlab page](https://23f-its-projekt.gitlab.io/team-2)

# Baggrund
Hvad er baggrunden for projektet:
## Softwarevirksomhed
**Afdelinger:**

- Udvikling
- Økonomi
- Testafdeling
- Support
- It drift 
- HR marketing

**Services**
Fælles:
- Tidsregistrering
- Mail
- VPN
- Ip tefeloni / soft phones / digital telefon
- Teams (zoom eller lignende)

Udvikling:
- Git
- CI/CD

Økonomi:
- Økonomi system (e-conomic?)


Support:
- VPN til kunder
- It drift: 


## Formål
Formålet med projektet er at skabe et overblik over en virksomheds netværk, hvilke services de benytter og hvordan sikkerheden i systemet bliver sat bedst muligt op.
## Mål
Projektet leverer følgende
### Nice to have
Enheds monitorering & logning
### Need to have
Netværksdiagram (Fysisk/Logisk)
Udvikle et til flere tekniske sikkerhedstiltag
Sikkerhedstest af tekniske produkter 
## Tidsplan
Bliver planlagt undervejs. Typisk efter projekt arbejde er slut, laves et nyt møde næste uge
### uge 8
Opgave: Planglægning dokument
Aftalt mødetid: Tirsdag (indtil 14:30) og onsdag (indtil 13:30) (efter valgfag/intro IT)
### uge 9
Aftalt mødettid: Fredag
### uge 10
Aftalt mødettid: Fredag
### uge 11
Aftalt mødettid: Fredag
### uge 12
Aftalt mødettid: Tirsdag
### uge 13
Aftalt mødettid: Tirsdag

## Organisation
Hvem er medlemmerne i teamet og hvad er deres roller i projektet.
Nils Andersen
Trine Nissen
Ulrik Vinther
## Risikostyring

| Risiko beskrivelse     | Risko håndtering                                                                                                                                                      | Konsekvensskala | Risikoskala |
|------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------|-------------|
| Sygdom (kortvarig)     | Gruppen fortsætter arbejdet, og informere den fraværende om fremskridt i projektet.                                                                                   | 2               | 4           |
| Sygdom (langvarig)     | Gruppen fortsætter arbejdet, og informere den fraværende om fremskridt i projektet. Derudover kontakt med vejlederne omkring bedste fremgangsmåde ud fra situationen. | 5               | 1           |
| Transport forsinkelser | Kontakt gruppen omkring forsinkelser, og hvornår forventet ankomst er.                                                                                                | 1               | 4           |
| Computer problemer     | Backup af enheder, dokumenter mm. dokumentation af fremgangsmåde, så man effektivt kan genskabe tabt data mm.                                                         | 4               | 2           |



Inspiration hentet her https://www.cfl.dk/artikler/risikoanalyse
## Interessenter
Her skrives resultatet af interessentanalyse
Inspiration kan hentes her https://www.cfl.dk/artikler/interessentanalyse
Hvem er opdragsgiver?
Lærer og teamet
Hvem skal anvende projektresultatet?
Teamet
Hvem skal acceptere projektresultatet?
Lærer
Hvem berøres (bemærker, generes, lider afsavn, får udbytte, får ændrede forhold)?
Softwarehuset
Hvem leverer indsats, viden, kunnen, ressourcer til projektet?
Teamet
Hvem skal acceptere, at projektets aktiviteter udføres, og måden de gennemføres på?
Lærerne 

## Kommunikation
Hvordan er det besluttet at kommunikere i projektet og er der ud fra interessant samt risiko analysen behov for at etablere kommunikation der understøtter dem.

### Kommunikationskanaler
gitlab issue board
discord
### Kommunikationsaktiviteter
Møder
