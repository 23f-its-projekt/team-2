# Info

Manuelt stepbystep til at opsætte audit rules udfra CIS BENCHMARKS og opsætning af Wazuh agent til at opsnappe audit.log

Er lavet til Debian 11 og følger kun benchmarks for audit 4.1.X (udfra IG1)

Læs venligst kommentare undervejs

Note: I en real case scenario skal dette automatiseres, hvilket CIS også påpeger. Dette er et POC.

## install auditd,aide & enable auditd


```bash
apt-get install auditd audispd-plugins -y
apt install aide aide-common -y
systemctl enable auditd
```
## Audit configuration IG1

```bash
# Ensure auditing for processes that start prior to auditd is enabled (Automated)
# Tilføj manuelt til /etc/default/grub
GRUB_CMDLINE_LINUX="audit=1"

# 4.1.1.4 Ensure audit_backlog_limit is sufficient (Automated)
# Tilføj manuelt til /etc/default/grub
GRUB_CMDLINE_LINUX="audit_backlog_limit=8192"
# Opdatere grub
update-grub

# 4.1.2.1 Ensure audit log storage size is configured (Automated)
# Tilføj manuelt til /etc/audit/auditd.conf
max_log_file = 32

# 4.1.2.2 Ensure audit logs are not automatically deleted
(Automated)
# tilføj manuelt til /etc/audit/auditd.conf
max_log_file_action = keep_logs

# 4.1.2.3 Ensure system is disabled when audit logs are full(Automated)
# tilføj manuelt til /etc/audit/auditd.conf:
space_left_action = email
action_mail_acct = root
admin_space_left_action = halt

# 4.1.3.1 Ensure changes to system administration scope (sudoers)is collected (Automated)
cat > etc/audit/rules.d/50-scope.rules<< EOF
touch /etc/audit/rules.d/50-scope.rules
-w /etc/sudoers -p wa -k scope
-w /etc/sudoers.d -p wa -k scope
EOF
augenrules --load

# 4.1.3.2 Ensure actions as another user are always logged(Automated)
cat > /etc/audit/rules.d/50-user_emulation.rules<< EOF
-a always,exit -F arch=b64 -C euid!=uid -F auid!=unset -S execve -k user_emulation
-a always,exit -F arch=b32 -C euid!=uid -F auid!=unset -S execve -k user_emulation
EOF
augenrules --load

# 4.1.3.3 Ensure events that modify the sudo log file are collected(Automated)
cat > /etc/audit/rules.d/50-sudo-log.rules<< EOF
-w /var/log/auth.log -p wa -k sudoaction
EOF
augenrules --load

# 4.1.3.4 Ensure events that modify date and time information are collected (Automated)
cat > /etc/audit/rules.d/50-time-change.rules<< EOF
-a always,exit -F arch=b64 -S adjtimex,settimeofday,clock_settime -k time-change
-a always,exit -F arch=b32 -S adjtimex,settimeofday,clock_settime -k time-echo-change
-w /etc/localtime -p wa -k time-change
EOF
augenrules --load

# 4.1.3.6 Ensure use of privileged commands are collected
# Der er problem med remidation for denne via CIS benchmarks. Dette er et alternativ chatgpt løsning
[ ! -f "/etc/audit/rules.d/50-privileged.rules" ] && touch "/etc/audit/rules.d/50-privileged.rules"; UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs); AUDIT_RULE_FILE="/etc/audit/rules.d/50-privileged.rules"; NEW_DATA=(); for PARTITION in $(findmnt -n -l -k -it $(awk '/nodev/ { print $2 }' /proc/filesystems | paste -sd,) | grep -Pv "noexec|nosuid" | awk '{print $1}'); do readarray -t DATA < <(find "${PARTITION}" -xdev -perm /6000 -type f | awk -v UID_MIN=${UID_MIN} '{print "-a always,exit -F path=" $1 " -F perm=x -F auid>=" UID_MIN " -F auid!=unset -k privileged"}'); for ENTRY in "${DATA[@]}"; do NEW_DATA+=("${ENTRY}"); done; done; readarray -t OLD_DATA < "${AUDIT_RULE_FILE}" 2>/dev/null; COMBINED_DATA=( "${OLD_DATA[@]}" "${NEW_DATA[@]}" ); printf '%s\n' "${COMBINED_DATA[@]}" | sort -u > "${AUDIT_RULE_FILE}"
augenrules --load

# 4.1.7 Ensure login and logout events are collected (Scored)
cat > /etc/audit/rules.d/50-login-logout.rules<< EOF
-w /var/log/faillog -p wa -k logins
-w /var/log/lastlog -p wa -k logins
EOF
augenrules --load

# 4.1.3.13 Ensure file deletion events by users are collected
# DNedenstående er en helt code block. Copy paste linje 89-96
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && printf "
-a always,exit -F arch=b64 -S rename,unlink,unlinkat,renameat -F
auid>=${UID_MIN} -F auid!=unset -F key=delete
-a always,exit -F arch=b32 -S rename,unlink,unlinkat,renameat -F
auid>=${UID_MIN} -F auid!=unset -F key=delete
" >> /etc/audit/rules.d/50-delete.rules || printf "ERROR: Variable 'UID_MIN'
is unset.\n"
augenrules --load


# 4.1.3.15 Ensure successful and unsuccessful attempts to use the chcon
# Nedenstående er en helt code block. Copy paste linje 102-107
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && printf "
-a always,exit -F path=/usr/bin/chcon -F perm=x -F auid>=${UID_MIN} -F
auid!=unset -k perm_chng
" >> /etc/audit/rules.d/50-perm_chng.rules || printf "ERROR: Variable
'UID_MIN' is unset.\n"
augenrules --load

# 4.1.3.16 Ensure successful and unsuccessful attempts to use the setfacl
# Nedenstående er en helt code block. copy paste lije 112-117
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && printf "
-a always,exit -F path=/usr/bin/setfacl -F perm=x -F auid>=${UID_MIN} -F
auid!=unset -k perm_chng
" >> /etc/audit/rules.d/50-perm_chng.rules || printf "ERROR: Variable
'UID_MIN' is unset.\n"
augenrules --load

# 4.1.3.17 Ensure successful and unsuccessful attempts to use the chacl command are recorded (Automated) 
# Nedenstående er en helt code block. Copy paste linje 112-127
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && printf "
-a always,exit -F path=/usr/bin/chacl -F perm=x -F auid>=${UID_MIN} -F
auid!=unset -k perm_chng
" >> /etc/audit/rules.d/50-perm_chng.rules || printf "ERROR: Variable
'UID_MIN' is unset.\n"
augenrules --load

# 4.1.3.18 Ensure successful and unsuccessful attempts to use the usermod command are recorded (Automated) 
# Nedenstående er en helt code block. Copy paste linje 132-137
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && printf "
-a always,exit -F path=/usr/sbin/usermod -F perm=x -F auid>=${UID_MIN} -F
auid!=unset -k usermod
" >> /etc/audit/rules.d/50-usermod.rules || printf "ERROR: Variable 'UID_MIN'
is unset.\n"
augenrules --load

# 4.1.3.19 Ensure kernel module loading unloading and modification is collected (Automated)
# Nedenstående er en helt code block. Copy paste linje 142-150
UID_MIN=$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)
[ -n "${UID_MIN}" ] && printf "
-a always,exit -F arch=b64 -S
init_module,finit_module,delete_module,create_module,query_module -F
auid>=${UID_MIN} -F auid!=unset -k kernel_modules
-a always,exit -F path=/usr/bin/kmod -F perm=x -F auid>=${UID_MIN} -F
auid!=unset -k kernel_modules
" >> /etc/audit/rules.d/50-kernel_modules.rules || printf "ERROR: Variable
'UID_MIN' is unset.\n"
augenrules --load

# 4.1.3.20 Ensure the audit configuration is immutable (Automated)
Nedenstående er en helt code block. Copy paste linje 155-156
printf -- "-e 2
" >> /etc/audit/rules.d/99-finalize.rules

# 4.1.4.1 Ensure audit log files are mode 0640 or less permissive
[ -f /etc/audit/auditd.conf ] && find "$(dirname $(awk -F "=" '/^\s*log_file/ {print $2}' /etc/audit/auditd.conf | xargs))" -type f \( ! -perm 600 -a ! -perm 0400 -a ! -perm 0200 -a ! -perm 0000 -a ! -perm 0640 -a ! -perm 0440 -a ! -perm 0040 \) -exec chmod u-x,g-wx,o-rwx {} +

# 4.1.4.2 Ensure only authorized users own audit log files
[ -f /etc/audit/auditd.conf ] && find "$(dirname $(awk -F "=" '/^\s*log_file/ {print $2}' /etc/audit/auditd.conf | xargs))" -type f ! -user root -exec chown root {} +

# 4.1.4.3 Ensure only authorized groups are assigned ownership of audit log files (Automated)
find $(dirname $(awk -F"=" '/^\s*log_file/ {print $2}' /etc/audit/auditd.conf | xargs)) -type f \( ! -group adm -a ! -group root \) -exec chgrp adm {} +
chgrp adm /var/log/audit/
sed -ri 's/^\s*#?\s*log_group\s*=\s*\S+(\s*#.*)?.*$/log_group = adm\1/' /etc/audit/auditd.conf
systemctl restart auditd

# 4.1.4.4 Ensure the audit log directory is 0750 or more restrictive
chmod g-w,o-rwx "$(dirname $(awk -F"=" '/^\s*log_file/ {print $2}' /etc/audit/auditd.conf))"

# 4.1.4.5 Ensure audit configuration files are 640 or more restrictive
find /etc/audit/ -type f \( -name '*.conf' -o -name '*.rules' \) ! -user root -exec chown root {} +

# 4.1.4.7 Ensure audit configuration files belong to group root
find /etc/audit/ -type f \( -name '*.conf' -o -name '*.rules' \) ! -group root -exec chgrp root {} +

# 4.1.4.8 Ensure audit tools are 755 or more restrictive (Automated)
chmod go-w /sbin/auditctl /sbin/aureport /sbin/ausearch /sbin/autrace /sbin/auditd /sbin/augenrules

# 4.1.4.9 Ensure audit tools are owned by root (Automated)
chown root /sbin/auditctl /sbin/aureport /sbin/ausearch /sbin/autrace /sbin/auditd /sbin/augenrules

# 4.1.4.10 Ensure audit tools belong to group root (Automated)
chmod go-w /sbin/auditctl /sbin/aureport /sbin/ausearch /sbin/autrace /sbin/auditd /sbin/augenrules
chown root:root /sbin/auditctl /sbin/aureport /sbin/ausearch /sbin/autrace /sbin/auditd /sbin/augenrules

# 4.1.4.11 Ensure cryptographic mechanisms are used to protect the integrity of audit tools (Automated)
# add to file /etc/aide/aide.conf
cat >> /etc/aide/aide.conf << EOF
/sbin/auditctl p+i+n+u+g+s+b+acl+xattrs+sha512
/sbin/auditd p+i+n+u+g+s+b+acl+xattrs+sha512
/sbin/ausearch p+i+n+u+g+s+b+acl+xattrs+sha512
/sbin/aureport p+i+n+u+g+s+b+acl+xattrs+sha512
/sbin/autrace p+i+n+u+g+s+b+acl+xattrs+sha512
/sbin/augenrules p+i+n+u+g+s+b+acl+xattrs+sha512
EOF
```

## Finish with Reload auditd
```bash
service auditd reload
```

## Konfigurer wazuh manager til at opsnappe audit.log

Der skal tilføjes til wazuh manager configuration filen (/var/ossec/etc/ossec.conf) følgende: 

```bash
<localfile>
    <location>/var/log/audit/audit.log</location>
    <log_format>audit</log_format>
</localfile>
```

Af god ordens skyld, reset wazuh agent daemon:

```bash
systemctl restart wazuh-agent
```
