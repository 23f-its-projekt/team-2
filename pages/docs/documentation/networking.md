# Access control list

Denne liste blev udarbejdet til at styre processen for netværkskommunikationsflow. Og herved sørge for at der kun tillades traffik gennem netværket der følger POLP på alt udover mangement netværket, da det er til sysadmins.

Listen er opsat sådan at fra zoner går ud af x-aksen, og til zoner går ned fra y-aksen. Fx. så er Traffic fra Trust til zone Trust Permit all traffic

|     |     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- | --- |
|     | Fra zone |     |     |     |     |     |
| Til zone | Trust | untrust | Printer | DMZ | Server local | Mangement |
| Trust | Permit all traffic | Deny all traffic | Deny all traffic | Deny all traffic | Deny all traffic | Permit all traffic |
| untrust | Permit all traffic | Deny all traffic | HTTP(S), DNS (til updates) | HTTP(S), DNS (til updates) | HTTP(S), DNS (til updates) | Permit all traffic |
| Printer | Printer port 9100 TCP | Deny all traffic | Deny all traffic | Deny all traffic | Deny all traffic | Permit all traffic |
| DMZ | Deny all traffic | allow webserver port 80; | Deny all traffic | Deny all traffic | Deny all traffic | Permit all traffic |
| Server local | SSH fil server port 22 (alle i trust), SSH webser port 22 (kun udvikling + support), wazuh agent port 1514-1515 | Deny all traffic | TCP port 1514-1515 Wazuh | TCP port 1514-1515 Wazuh | TCP port 1514-1515 Wazuh | Permit all traffic |
| Mangement local | Deny all traffic | Deny all traffic | Deny all traffic | Deny all traffic | Deny all traffic | Permit all traffic |

**Hvad kunne forbedres**

Timers der fx. Opsætter en åben tidsrammer hvor servere og andet kristik for lov til at tilgå internettet til at opdatere. Derefter bliver der lukket for de porte igen via. router/firewall. PFsense kan godt dette. Så det ville være en fobedring af POLP.

## Netværks diagrammet

Det logiske netværksdiagram indeholder udviklingshuset enheder på netværket. Det er opdelt i diverse zone, hvor forskellige medarbejder arbejder indenfor, samt andet udstyr såsom servere er i deres egen zoner


![netværksdiagram](../images/networkingdiagramv2.svg)




