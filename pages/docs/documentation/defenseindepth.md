# Hvordan har vi brugt DiD

I arbejdet med at undersøge hvordan vi bedst muligt kan sikre virksomheden, har vi haft fokus på defense in depth. Det betyder, at vi både har haft fokus på at implementere sikkerhedsforanstaltninger i udviklingshuset netværk blandt andet i form af netværks segmentering, samt i enheders operativsystemer i form af begrænsning af brugerrettigheder for at følge principle of least privilege. Vi har i vores tilfælde dog ikke kigge på f.eks. virksomhedens fysiske sikkerhed eller deres applikations sikkerhed, da dette ikke ligger indenfor det afgrænset område som vi har beskæftiget os med.

## Modellen

![Defense in depth UCL modellen](../images/did.svg)
